package com.example.web_app;

public class Student {
    private String studentID;
    private String studentName;
    private String department;
    private int mark;

    public Student(String studentID, String studentName, String department, int mark) {
        this.studentID = studentID;
        this.studentName = studentName;
        this.department = department;
        this.mark = mark;
    }

    public String getStudentID() {
        return studentID;
    }

    public String getStudentName() {
        return studentName;
    }

    public String getDepartment() {
        return department;
    }

    public int getMark() {
        return mark;
    }
}

