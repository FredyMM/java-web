package com.example.web_app;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/getStudentName")
public class StudentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    // Daftar objek hardcoded
    private static List<Student> students = new ArrayList<>();

    static {
        students.add(new Student("101", "John Doe", "Computer Science", 85));
        students.add(new Student("102", "Jane Smith", "Mathematics", 75));
        students.add(new Student("103", "Bob Johnson", "Physics", 60));
        students.add(new Student("104", "Alice Brown", "Chemistry", 55));
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userId = request.getParameter("userId");
        String studentName = getStudentName(userId);
        response.setContentType("text/plain");
        response.getWriter().write(studentName);
    }

    private String getStudentName(String userId) {
        for (Student student : students) {
            if (student.getStudentID().equals(userId)) {
                return student.getStudentName();
            }
        }
        return "Not Found";
    }
}

