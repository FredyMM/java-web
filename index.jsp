<!DOCTYPE html>
<html>
<head>
    <title>Student List</title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>
<h1>Student List</h1>
<table border="1">
    <thead>
    <tr>
        <th>Student ID</th>
        <th>Student Name</th>
        <th>Department</th>
        <th>Mark</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${students}" var="student">
        <tr>
            <td>${student.studentID}</td>
            <td>${student.studentName}</td>
            <td>${student.department}</td>
            <td>${student.mark}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<label for="userId">User ID:</label>
<input type="text" name="userId" id="userId" required>
<script>
    $(document).ready(function() {
        $('input[name="userId"]').on('blur', function() {
            var userId = $(this).val();
            // Lakukan request ke Servlet untuk mendapatkan Nama Mahasiswa berdasarkan ID
            $.ajax({
                url: 'getStudentName',
                type: 'POST',
                data: { userId: userId },
                success: function(response) {
                    alert('Student Name: ' + response);
                },
                error: function(xhr, status, error) {
                    alert('Error while fetching Student Name.');
                }
            });
        });
    });
</script>
</body>
</html>
